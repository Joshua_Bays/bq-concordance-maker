/*
	Concordance Creator
	Author: Joshua Bays (https://gitlab.com/Joshua_Bays)
	License: GPL2

TODO: Replace certain functions with std string operations
TODO: Update annotations
*/

#include <deque>
#include <fstream>
#include <string>
#include <stdio.h>

#define delimChar '|'

typedef struct wordStruct{
	std::string word;
	std::deque<unsigned> indices;
}wordStruct;

int find_first_character_in_str(const std::string &str, const char ch); // Return the first index of a specified character within a provided string (-1 if not found)
std::string lower_str(std::string str); // Make the string lowercase
std::string hl_word(std::string str, std::string word, unsigned inst); // Surrounds a substring of text with *'s
std::string substr(std::string str, unsigned low, unsigned high); // Returns a substring from within a provided string
unsigned find_freq_in_deque(std::deque<unsigned> dqe, unsigned var); // Returns how many times a specified number is found within a deque
void lazy_sort_deque(std::deque<wordStruct> &dqe); // Sub-optimal sorting function that gets the job done
void parse_line(const std::string &str, std::deque<std::string> &ret); // Break the provided string into a deque of individual words

int main(int argc, char *argv[]){
	// CLI handling
	if(argc != 4){ printf("Too many or too few arguments, exiting.\n"); return 1; }
	int min = std::atoi(argv[2]); int max = std::atoi(argv[3]);
	
	// Set up main variables
	std::deque<std::deque<std::string>> verses; std::deque<std::string> dqe;
	std::string s1; std::string s2; int breakPos;
	
	// Read the file and break it into 2 based on the delimiter character
	std::ifstream readFile; std::string line;
	readFile.open(argv[1], std::ios::in);
	while(getline(readFile, line)){
		breakPos = find_first_character_in_str(line, delimChar);
		if(breakPos != -1){ // Lines without the delimiter character will not be processed
			s1 = ""; s2 = "";
			for(unsigned i = 0; i < breakPos; i++){ s1 += line[i]; }
			for(unsigned i = breakPos + 1; i < line.size(); i++){ s2 += line[i]; }
			dqe = {s1, s2}; verses.push_back(dqe);
		}
	}
	
	std::deque<wordStruct> wordList; bool newWord;
	for(unsigned i = 0; i < verses.size(); i++){
		dqe = {}; parse_line(verses[i][1], dqe);
		for(unsigned j = 0; j < dqe.size(); j++){
			newWord = 1;
			for(unsigned k = 0; k < wordList.size() && newWord; k++){
				if(wordList[k].word == dqe[j]){ newWord = 0; wordList[k].indices.push_back(i); }
			}
			if(newWord){ wordList.push_back({dqe[j], {i}}); }
		}
	}
	
	lazy_sort_deque(wordList);
	
	for(unsigned i = 0; i < wordList.size(); i++){
		if(wordList[i].indices.size() >= min && wordList[i].indices.size() <= max){
			printf("%s (%u)\n", wordList[i].word.c_str(), wordList[i].indices.size());
			for(unsigned j = 0; j < wordList[i].indices.size(); j++){
				for(unsigned k = 1; k <= find_freq_in_deque(wordList[i].indices, wordList[i].indices[j]); k++){
					printf("\t%s: %s\n", verses[wordList[i].indices[j]][0].c_str(), hl_word(verses[wordList[i].indices[j]][1], wordList[i].word, k).c_str());
					j -= 1; j += k;
				}
			}
			printf("\n");
		}
	}
	
	return 0;
}

int find_first_character_in_str(const std::string &str, const char ch){
	for(unsigned i = 0; i < str.size(); i++){
		if(str[i] == ch){ return i; }
	}
	return -1;
}

std::string lower_str(std::string str){
	for(unsigned i = 0; i < str.size(); i++){
		if(str[i] >= 'A' && str[i] <= 'Z'){ str[i] += 32; }
	}
	return str;
}

std::string hl_word(std::string str, std::string word, unsigned inst){
	unsigned counter = 0; std::string ret = "";
	std::string compStr = lower_str(str); word = lower_str(word);
	for(unsigned i = 0; i < str.size() - word.size() + 1; i++){
		if(substr(compStr, i, i + word.size() - 1) == word && (i + word.size() == compStr.size() || !(compStr[i + word.size()] >= 'a' && compStr[i + word.size()] <= 'z')) && (i == 0 || !(compStr[i - 1] >= 'a' && compStr[i - 1] <= 'z'))){
			counter++;
			if(counter == inst){
				ret += "*"; ret += substr(str, i, i + word.size() - 1); ret += "*";
				ret += substr(str, i + word.size(), str.size() + 1);
				break;
			}
			else{ ret += str[i]; }
		}
		else{ ret += str[i]; }
	}
	return ret;
}

std::string substr(std::string str, unsigned low, unsigned high){
	std::string ret = "";
	for(unsigned i = low; i <= high; i++){ ret += str[i]; }
	return ret;
}

unsigned find_freq_in_deque(std::deque<unsigned> dqe, unsigned var){
	unsigned ret = 0;
	for(unsigned i = 0; i < dqe.size(); i++){
		if(dqe[i] == var){ ret++; }
	}
	return ret;
}

void lazy_sort_deque(std::deque<wordStruct> &dqe){
	wordStruct tmp;
	for(unsigned i = 0; i < dqe.size(); i++){
		for(unsigned j = i + 1; j < dqe.size(); j++){
			if(dqe[i].word > dqe[j].word){ tmp = dqe[j]; dqe[j] = dqe[i]; dqe[i] = tmp; }
		}
	}
}

void parse_line(const std::string &str, std::deque<std::string> &ret){
	std::string word = "";
	for(unsigned i = 0; i < str.size(); i++){
		switch(str[i]){
			case 'a' ... 'z':
			case '-':
			case '\'':
				word += str[i];
				break;
			case 'A' ... 'Z':
				word += str[i] + 32;
				break;
			default:
				if(word.size() > 0){ ret.push_back(word); word = ""; }
				break;
		}
	}
	if(word.size() > 0){ ret.push_back(word); word = ""; }
}

